import glob, os

for file in glob.glob("log*"):
	with open(file, 'r') as logFile:
		lines = logFile.readlines()

	with open(file, 'w') as logFile:
		for count, line in enumerate(lines):
			if (count < len(lines) - 1 and lines[count + 1].startswith('unable to get maxutts')) or lines[count].startswith('unable to get maxutts'):
				continue
			else:
				logFile.write(line)


