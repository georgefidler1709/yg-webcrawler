from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from random import randint
import time
import os
import subprocess
import boto3
import botocore
import uuid

with open('test_output.txt', 'a') as testFile:
          testFile.write('start\n')
          testFile.flush()

BUCKET_NAME = 'audio-crawler'

s3 = boto3.resource('s3')
s3 = boto3.resource('s3',
         aws_access_key_id = "AKIAJKLCTRVKDOSTER5A",
         aws_secret_access_key = "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" )

if not os.path.isfile("log.txt"):
  open("log.txt", 'a').close()

options = Options()
profile = webdriver.FirefoxProfile()
profile.set_preference("general.useragent.override", str(randint(1,1000000)))
driver = webdriver.Firefox(firefox_profile=profile, firefox_options=options)

with open('word_list_alc' , 'r') as wordFile, open("log.txt" , 'r+') as logFile:
  used_words = logFile.read().strip().split()
  for word in wordFile:
    word = word[:-1]
    if word in used_words:
      continue
    logFile.write(word + '\n')
    
    try:
      with open('test_output.txt', 'a') as testFile:
          testFile.write('trying ' + word + '\n')
          testFile.flush()
          driver.get("https://youglish.com/search/" + word + "/us")
          delay = randint(400, 800)
          delay = delay / 100
          time.sleep(delay)
          driver.switch_to_default_content()
          html = driver.page_source
          total_for_word = -1
          text = str(html)
          testFile.write(text + '\n\n\nBANANANANA\n\n\n')
          testFile.flush()
          title = text.find("<title>")
          text = text[title + len("<title>"):]
          testFile.write(text + '\n\n\nBANANANANA\n\n\n')
          testFile.flush()
          end = text.find(" ")
          with open('test_output.txt', 'a') as testFile:
            testFile.write(text[:end])
            testFile.flush()
          if text[:end] == 'Bot':
            continue
          total_for_word = int(text[:end])
          logFile.flush()
    except:
      logFile.write("unable to get maxutts for " + word + '\n')
      logFile.flush()
      driver.quit()
      options = Options()
      profile = webdriver.FirefoxProfile()
      profile.set_preference("general.useragent.override", str(uuid.uuid4()))
      driver = webdriver.Firefox(firefox_profile=profile, firefox_options=options)
      continue

    utts_collected = 0
    with open(word + '_total.txt', 'w') as outFile:
      while utts_collected < 50 and utts_collected < total_for_word:
        try:
          frame = driver.find_element_by_id('player')
          driver.switch_to_frame(frame)

          html = driver.page_source

          text = str(html)
          start = text.find('href=\"https://www.youtube.com')
          start = start + len('href="')
          url = text[start:]

          start = url.find('href=\"https://www.youtube.com')
          start = start + len('href="')
          url = url[start:]
          end = url.find('"')
          url = url[:end]
          outFile.write(url + '\n')

          driver.switch_to_default_content()
          webdriver.common.action_chains.ActionChains(driver).key_down(Keys.CONTROL).send_keys(Keys.ARROW_RIGHT).key_up(Keys.CONTROL).perform()
          delay = randint(400, 800)
          delay = delay / 100
          time.sleep(delay)
          utts_collected += 1
        except:
          logFile.write("unable to extract link for " + word + " " + utts_collected + '\n')
          logFile.flush()
          driver.quit()
          break

    subprocess.call(["sort" , word + "_total.txt", "-o", word + "_total.txt"])
    with open(word + ".txt", 'w') as sortFile:
      subprocess.Popen(["uniq", word + "_total.txt"] , stdout=sortFile)
    s3.meta.client.upload_file(word + '.txt', BUCKET_NAME, "US/YT-links-by-word/" + word + '.txt')
    os.remove(word + '.txt')
    subprocess.Popen('rm *_total.txt', shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
    with open('test_output.txt', 'a') as testFile:
          testFile.write('_total files removed\n')
          testFile.flush()

driver.quit()
