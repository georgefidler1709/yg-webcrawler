from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from random import randint
import time
import os
import subprocess
import boto3
import botocore
import uuid
import argparse
import threading

BUCKET_NAME = 'audio-crawler-2019'
HOME = "/home/gej/"
#HOME = "/media/gej/gej_work_drive/Programs/LC/"

s3 = boto3.resource('s3')
s3 = boto3.resource('s3',
         aws_access_key_id = "AKIAJHYTXQ5N2N5LMFPQ",
         aws_secret_access_key = "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" )

log_lock = threading.Lock()


def yt_links_by_word(word_list, region):
  options = Options()
  options.add_argument("--headless")
  profile = webdriver.FirefoxProfile()
  profile.set_preference("general.useragent.override", str(randint(1,1000000)))
  driver = webdriver.Firefox(firefox_profile=profile, firefox_options=options)

  for word in word_list:
    print("restarting")
    log_lock.acquire()
    with open(HOME + "webcrawler/word_log.txt" , 'r+') as logFile:
      logFile.write(word + '\n')
      logFile.flush()
    log_lock.release()
  #try:
    print("https://youglish.com/search/" + word + "/" + region)
    driver.get("https://youglish.com/search/" + word + "/" + region)
    delay = randint(400, 800)
    delay = delay / 100
    time.sleep(delay)
    #driver.switch_to_default_content()
    html = driver.page_source
    total_for_word = -1
    text = str(html)
    title = text.find("<title>")
    text = text[title + len("<title>"):]
    end = text.find(" ")
    total_for_word = int(text[:end])
  #except:
    #logFile.write("unable to get maxutts for " + word + '\n')
    #logFile.flush()
    #continue

    utts_collected = 0
    with open(HOME + 'webcrawler/' + word + '_total.txt', 'w') as outFile:
      while utts_collected < 50 and utts_collected < total_for_word:
        try:
          frame = driver.find_element_by_id('player')
          driver.switch_to_frame(frame)

          html = driver.page_source

          text = str(html)
          start = text.find('href=\"https://www.youtube.com/watch')
          start = start + len('href="')
          url = text[start:]

          start = url.find('href=\"https://www.youtube.com/watch')
          start = start + len('href="')
          url = url[start:]
          end = url.find('"')
          url = url[:end]
          print(word + ": " + str(utts_collected) + ": " + url)
          outFile.write(url + '\n')

          driver.switch_to_default_content()
          webdriver.common.action_chains.ActionChains(driver).key_down(Keys.CONTROL).send_keys(Keys.ARROW_RIGHT).key_up(Keys.CONTROL).perform()
          delay = randint(200, 400)
          delay = delay / 100
          time.sleep(delay)
          utts_collected += 1
        except:
          # logFile.write("unable to extract link for " + word + " " + str(utts_collected) + '\n')
          # logFile.flush()
          #driver.quit()
          break
    print("sorting")
    subprocess.call(["sort" , HOME + 'webcrawler/' + word + "_total.txt", "-o", HOME + 'webcrawler/' + word + "_total.txt"])
    with open(HOME + 'webcrawler/' + word + ".txt", 'w+') as sortFile:
      subprocess.Popen(["uniq", HOME + 'webcrawler/' + word + "_total.txt"] , stdout=sortFile)
    print("uploading to s3")
    s3.meta.client.upload_file(HOME + 'webcrawler/' + word + '.txt', BUCKET_NAME, "UK/YT-links-by-word/" + word + '.txt')
    print("removing")
    os.remove(HOME + 'webcrawler/' + word + '.txt')
    subprocess.Popen('rm ' + HOME + 'webcrawler/*_total.txt', shell=True,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)

  driver.quit()




def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("-t", help="number of threads to run on.", default=4)
  parser.add_argument("-region", help="english speaking region for samples to come from e.g. US, UK, AUS", default="us")
  args = parser.parse_args()
  region = args.region
  num_threads = int(args.t)

  if not os.path.isfile(HOME + "webcrawler/word_log.txt"):
    open(HOME + "webcrawler/word_log.txt", 'a').close()

  full_word_list = []
  with open(HOME + 'webcrawler/word_list.txt' , 'r') as wordFile, open(HOME + 'webcrawler/word_log.txt', 'r+') as logFile:
    used_words = logFile.read().strip().split()
    for word in wordFile:
      word = word.strip()
      if word in used_words:
        continue
      else:
        full_word_list.append(word)

    chunk_size = int(len(full_word_list)/num_threads)
    for i in range(0, len(full_word_list), chunk_size):
      thread_word_list = full_word_list[i : i + chunk_size]
      thread = threading.Thread(target=yt_links_by_word, args=(thread_word_list, region))
      thread.start()

   
if __name__ == "__main__":
  exit(main())
